import { WebPlugin } from '@capacitor/core';

import type { DayClockUpgradePlugin } from './definitions';

export class DayClockUpgradeWeb
  extends WebPlugin
  implements DayClockUpgradePlugin {
  upgrade(): Promise<{ value: string; }> {
    throw new Error('Method not implemented.');
  }
  
}
