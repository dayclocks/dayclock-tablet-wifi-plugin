export interface DayClockUpgradePlugin {
  upgrade(): Promise<{ value: string }>;
}
